from typing import Optional

from fastapi import FastAPI, HTTPException, Request, status, Form, Header
from pydantic import BaseModel, Field
from uuid import UUID
from starlette.responses import JSONResponse


# custom exception
class NegativeNumberException(Exception):
    def __init__(self, books_to_return):
        self.books_to_return = books_to_return


app = FastAPI()


# extend from baseModel, automatic validation
class Book(BaseModel):
    id: UUID
    title: str = Field(min_length=1)
    author: str = Field(min_length=1, max_length=100)
    description: Optional[str] = Field(title="Description of the book",
                                       max_length=200,
                                       min_length=1)
    rating: int = Field(gt=-1, lt=101)  # gt = > lt = <

    class Config:
        # dictionary, show an example for pre-defined data
        schema_extra = {
            "example": {
                "id": "5ecce15a-21db-4c99-b9f3-3444a12e7d22",
                "title": "Programming pro",
                "author": "John Lenon",
                "description": "It's very good.",
                "rate": 50
            }
        }


class BookNoRating(BaseModel):
    id: UUID
    title: str = Field(min_length=1)
    author: str = Field(min_length=1, max_length=100)
    description: Optional[str] = Field(
        None, title="Description of the book",
        max_length=200,
        min_length=1)


BOOKS = []


# handle the exception when NegativeNumberException exception class raised
@app.exception_handler(NegativeNumberException)
async def negative_number_exception_handler(request: Request,
                                            exception: NegativeNumberException):
    return JSONResponse(
        status_code=418,
        content={"message": f"Hey, why do you want {exception.books_to_return} "
                            f"books? You need to read more!"}
    )


@app.post("/books/login")
async def book_login(username: str = Form(), password: str = Form()):
    return {"username": username, "password": password}


@app.get("/header")
async def read_header(random_header: Optional[str] = Header(None)):
    return {"Random_Header": random_header}


@app.get("/book/{book_id}")
async def read_book(book_id: UUID):
    for i in BOOKS:
        if i.id == book_id:
            return i
    raise raise_item_cannot_be_found_exception()


# change the response base on BookNoRating class fields (filter rating)
@app.get("/book/rating/{book_id}", response_model=BookNoRating)
async def read_book_no_rating(book_id: UUID):
    for i in BOOKS:
        if i.id == book_id:
            return i
    raise raise_item_cannot_be_found_exception()


@app.get("/")
async def read_all_books(books_to_return: Optional[int] = None):
    if books_to_return and books_to_return < 0:
        raise NegativeNumberException(books_to_return=books_to_return)

    if len(BOOKS) < 1:
        create_books_no_api()
    if books_to_return and len(BOOKS) >= books_to_return > 0:
        i = 1
        new_books = []
        while i <= books_to_return:
            new_books.append(BOOKS[i - 1])
            i += 1
        return new_books
    return BOOKS


# Fake function for Assignment
@app.post("/books/login/")
async def book_login_fake_authentication(book_id: int, username: Optional[str] = Header(None), password: Optional[str] = Header(None)):
    # book_id refer to location not uuid
    if username == "FastAPIUser" and password == "test1234":
        return BOOKS[book_id]
    return 'Invalid User!!'


@app.post("/", status_code=status.HTTP_201_CREATED)
async def create_book(book: Book):  # query parameter baseModel object
    BOOKS.append(book)
    return book


@app.put("/{book_id}")
async def update_book(book_id: UUID, book: Book):
    counter = 0

    for x in BOOKS:
        if x.id == book_id:
            BOOKS[counter] = book
            return BOOKS[counter]
        counter += 1
    raise raise_item_cannot_be_found_exception()


@app.delete("/{book_id}")
async def delete_book(book_id: UUID):
    counter = 0
    for x in BOOKS:
        if x.id == book_id:
            del BOOKS[counter]
            return f'ID: {book_id} deleted!'
        counter += 1
    raise raise_item_cannot_be_found_exception()


def create_books_no_api():
    book_1 = Book(id="5ecce15a-21db-4c99-b9f3-3444a12e7d21",
                  title="title 1",
                  author="author 1",
                  description="description 1",
                  rating=1)
    book_2 = Book(id="8ecce15a-21db-4c99-b9f3-3444a12e7d21",
                  title="title 2",
                  author="author 2",
                  description="description 2",
                  rating=2)
    book_3 = Book(id="35cce15a-21db-4c99-b9f3-3444a12e7d21",
                  title="title 3",
                  author="author 3",
                  description="description 3",
                  rating=3)
    book_4 = Book(id="5ecce15a-21d4-4c99-b9f3-3444a12e7d21",
                  title="title 4",
                  author="author 4",
                  description="description 4",
                  rating=4)

    BOOKS.append(book_1)
    BOOKS.append(book_2)
    BOOKS.append(book_3)
    BOOKS.append(book_4)


def raise_item_cannot_be_found_exception():
    return HTTPException(status_code=404,
                         detail="Book not found",
                         headers={"X-Header-Error":
                                      "Nothing to be seen at the UUID"})
