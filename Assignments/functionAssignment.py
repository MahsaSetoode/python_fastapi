"""
- Create a function that takes in 3 parameters(firstname, lastname, age) and

returns a dictionary based on those values
"""


def create_dictionary(firstname, lastname, age):
    my_dictionary = {
        "firstName": firstname,
        "lastName": lastname,
        "age": age
    }
    return my_dictionary


user = create_dictionary("mahsa", "setoode", 23)
print(user)
