"""
- Create a variable grade holding an integer between 0 - 100
- Code if, elif, else statements to print the letter grade of the number grade variable
Grades:
A = 90 - 100
B = 80 - 89
C = 70-79
D = 60 - 69
F = 0 - 59
Example:

if grade = 87 then print('B')
"""
grade = int(input("Please enter a number between 0 - 100:"))

if 90 <= grade <= 100:
    print("A")
elif 80 <= grade < 90:
    print("B")
elif 70 <= grade <= 79:
    print("C")
elif 60 <= grade <= 69:
    print("D")
elif 0 <= grade <= 59:
    print("F")
else:
    print("Wrong input!")
