from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# SQLALCHEMY_DATABASE_URL = "sqlite:///./todos.db"
SQLALCHEMY_DATABASE_URL = "postgresql://postgres:mahsa12345@localhost/TodoApplicationDatabse"
# SQLALCHEMY_DATABASE_URL = "mysql+pymysql://root:Mahsa@123456!@127.0.0.1:3306/todoapp"

# create engine
engine = create_engine(
    # SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
    SQLALCHEMY_DATABASE_URL
)

# create session
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# create the database
Base = declarative_base()

