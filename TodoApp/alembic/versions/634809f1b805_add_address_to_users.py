"""add address to users

Revision ID: 634809f1b805
Revises: aefb7c3ea540
Create Date: 2023-07-29 19:19:38.617666

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '634809f1b805'
down_revision = 'aefb7c3ea540'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('users', sa.Column('address_id', sa.Integer(), nullable=True))
    op.create_foreign_key('address_users_fk', source_table="users", referent_table="address",
                          local_cols=['address_id'], remote_cols=["id"], ondelete="CASCADE")


def downgrade() -> None:
    op.drop_constraint('address_users_fk', table_name="users")
    op.drop_column('users', 'address_id')

