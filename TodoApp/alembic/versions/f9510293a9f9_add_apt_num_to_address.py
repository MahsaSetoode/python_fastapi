"""add apt_num to address

Revision ID: f9510293a9f9
Revises: 634809f1b805
Create Date: 2023-07-29 20:34:36.847370

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f9510293a9f9'
down_revision = '634809f1b805'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('address', sa.Column('apt_num', sa.Integer(), nullable=True))


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('address', 'apt_num')
